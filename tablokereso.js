﻿// tablokereso.js
console.log("Hello, world!");

//itt már elvileg össze vannak concat-olva a tömbök egy tablok_osszes tömbbé

//kiszedjük az esetleges undefined elemeket
tablok_osszes = tablok_osszes.filter(function( element ) {return element !== undefined;});

//rendezzük a ballagás éve szerint csökkenő rendbe
tablok_osszes.sort(function(a,b) {return (a[2] > b[2]) ? -1 : ((b[2] > a[2]) ? 1 : 0);} );

var szint = 0;

var befEvValaszto = document.getElementById("befEvValaszto");
var d = new Date();
var n = d.getFullYear();
befEvValaszto.innerHTML = '<option value="0" selected="selected"><i>mind</i></option>';
for(var i=(n>=2018,n,2018); i>=1893; i--){
  befEvValaszto.innerHTML += '<option value="' + i + '">' + i + '</option>';
}

var tabloValaszto = document.getElementById("tabloValaszto");
befEvSzures();

function befEvSzures(){
  var ev = befEvValaszto.value -0;
  tabloValaszto.innerHTML = "";
  for(var i=0; i<tablok_osszes.length; i++){
    if(ev == 0 || ev == tablok_osszes[i][2]){
      // a választóban már csak ezek jelennek meg
      tabloValaszto.innerHTML += '<option value="' + i + '">' + tablok_osszes[i][0] + ' ' + tablok_osszes[i][1] + '-' + tablok_osszes[i][2] + '</option>';
    }
  }
  if(tabloValaszto.innerHTML == "") tabloValaszto.innerHTML = '<option disabled><i>nincs a szűrésnek megfelelő tabló...</i></option>';
}

// tabló kiválasztásakor
function valaszt(){
  var kivalasztott = tablok_osszes[tabloValaszto.value-0]
  // meghatározzuk, melyik szinten van
  szint = kivalasztott[6];
  // meghatározzuk a fájlt
  var ujkep = document.getElementById("tablokep");
  ujkep.src = "tablo_600/" + kivalasztott[3];
  // generálunk egy linket megtekintéshez
  document.getElementById("tabloLink").innerHTML = '<a href="tablo_1080/' + kivalasztott[3] + '" target="_blank">' + kivalasztott[0] + ' ' + kivalasztott[1] + '-' + kivalasztott[2] + '</a>';
  // kirajzoljuk a szintet
  rajzol();
}

// megjelenítendő szint alaprajz váltásához
function szintvalto(mennyi){
  var ujszint = szint + mennyi;
  if(-1 <= ujszint && ujszint <= 2 && szint!=ujszint){
    szint = ujszint;
    rajzol();
  }
}

// c egy DOM-ból kapott objektum, a rajzvászon
var c = document.getElementById("myCanvas");

// ctx tulajdonképpen a rajzolást végző objektum, kb. mint egy "ecset"
var ctx = c.getContext("2d");

window.onload = function() {
  rajzol();
};

function rajzol(){
  
  var img = document.getElementById("alaprajz"+szint);
  
  // drawImage paraméterei: (kép, x, y, szélesség, magasság)
  // (utóbbi kettőt nem muszáj) x, y a canvas bal felső sarkából számolva
  ctx.drawImage(img, 0,0);
  
  if(tabloValaszto.value != ""){
    
    setTimeout(function(){ 
      var tablokep = document.getElementById("tablokep");
      var w = tablokep.naturalWidth; // a kép eredeti szélessége
      var h = tablokep.naturalHeight; // a kép eredeti magassága (általában 600, mert a tablo_600 mappában van)
      
      // zsugorítjuk a képet, hogy a szélessége 500 legyen
      h *= (500/w);
      w = 500;
      
      // tovább zsugorítjuk, ha a magassága véletlenül 355-nél nagyobb
      if(h>355){
        w *= (355/h);
        h = 355;
      }
      
      ctx.drawImage(tablokep, 10,10, w,h);
    }, 200);
    
  }

  for(var i=0; i<tablok_osszes.length; i++){    
    
    // akkor rajzolom a köröcskéjét a tablónak, ha a jelenlegi szinten van
    if(tablok_osszes[i][6] == szint){
      ctx.beginPath();
      // arc paraméterei: (x, y, r, kezdőszög, zárószög)
      var x = tablok_osszes[i][4];
      var y = tablok_osszes[i][5];
      ctx.arc(x, y, 5, 0, 2*Math.PI); // kör
      ctx.strokeStyle = "red";
      ctx.stroke();
      
      // ha történetesen ki van választva, ki is töltjük a kört
      if(i == tabloValaszto.value && tabloValaszto.value != ""){
        ctx.fillStyle = "red";
        ctx.fill();
      }
    } 
  }
}

// HTML-be: <canvas id= ...stb... onclick="showCoords(event)"></canvas>
function showCoords(event) {
    var rect = event.target.getBoundingClientRect();
    
    var x = Math.floor(event.clientX - rect.left);
    var y = Math.floor(event.clientY - rect.top);
    console.log("X,Y: " + x + ", " + y);
}
