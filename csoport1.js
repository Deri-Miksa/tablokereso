﻿/*csoport1.jp
 *
 *készítették: Kiefer Dániel(feltértépezés), Szemerédi Richárd(tömbök, feltértépezés), Zsbiók Márk(tömbök)
 *épület szintje: 1. emelet, Díszteremtől balra
 */
 
 /*
    fájl meghatározásánál ügyeljenek a szóközökre!
    
    X,Y pozíciók meghatározásánál teljes képernyő, console alulra dokkolva,
    és próbálják ki, hogy mit ír ki a konzol egérkattintásra:
      a bal felső sarok kb. 0,0
      a jobb alsó sarok kb. 800,600
    --> csak ekkor lesz jó a konzolból leolvasott érték!
    
    nevek írásánál minden egyes tablok elem alatt párhuzamosan legyen egy tömbelem a neveknek. pl.:
    tablok[12] = [...];
    nevek[12] = "Alfa Áron, Béta Barnabás, Gamma Gábor";
 */
 
var tablok = [];
var nevek = []; // EZT fájlonként

tablok[0] = ["Déri Miksa Gépipari Technikum",1963,1967,"1967. össz. 001_0316.jpg",293,479,1];
nevek[0] = "Alfa Áron, Béta Barnabás, Gamma Gábor";
tablok[1] = ["Déri Miksa Gépipari Technikum",1959,1963,"1963.össz.  001_0312.jpg",256,479,1];
tablok[2] = ["A Déri Miksa Szakközépiskola V. Osztálya",1986,1987,"1987. V. évf.Techn. nap. 001_0308.jpg",252,530,1];
tablok[3] = ["Déri Miksa Ipari Szakközépiskola",2001,2005,"2005.B  001_0310.jpg",252,549,1];
tablok[4] = ["Déri Miksa Ipari Szakközépiskola",2000,2004,"2004.A 001_0300.jpg",236,531,1];
tablok[5] = ["Déri Miksa Ipari Szakközépiskola",1998,2002,"2002.B  001_0302.jpg",236,547,1];
tablok[6] = ["Déri Miksa Ipari Szakközépiskola",1993,1997,"1997.E  001_0303.jpg",193,531,1];
tablok[7] = ["Déri Miksa Ipari Szakközépiskola",1992,1996,"1996.D  001_0306.jpg",193,547,1];
tablok[8] = ["Déri Miksa Ipari Szakközépiskola Gépgyártástechnológia Számítástechnika Szak",1993,1997,"1997.A  001_0286.jpg",154,531,1];
tablok[9] = ["Déri Miksa Ipari Szakközépiskola",1993,1997,"1997.B  001_0287.jpg",154,547,1];
tablok[10] = ["Déri Miksa Ipari Szakközépiskola",1997,2001,"2001.B   001_0273.jpg",111,531,1];
tablok[11] = ["Déri Miksa Ipari Szakközépiskola",2000,2004,"2004.D  001_0275.jpg",111,547,1];
tablok[12] = ["SZISZSZI Déri Miksa Tagintézménye Elektronika-Elektrotechnika",2010,2014,"2014.D  001_0298.jpg",236,500,1];
tablok[13] = ["Déri Miksa Gépipari Technikum",1966,1970,"1970.  össz.  001_0296.jpg",211,500,1];
tablok[14] = ["Déri Miksa Ipari Szakközépiskola",1992,1996,"1996.A   001_0293.jpg",194,500,1];
tablok[15] = ["Déri Miksa Ipari Szakközépiskola",1994,1998,"1998.E  001_0379.jpg",171,485,1];
nevek[15] = "Czeller Ákos,Bata István,Berenatai Csongor,Huszti Roland,Makra Tamás,Kis László,Jójárt Tamás,Mészáros Dániel,Gémesi Roland,Fodor Gábor,Papdi Gábor,Engi Tamás ,Bodó Imre ,Mandák István,Gonda Gábor ,Bába Mihály,Balogh Levente,Magyar Gyula,Lőrincz Péter,Németh Roland,Mátyás Péter Pál,Módra György,Kismárton Zoltán,Ocskó Zsolt,Pásztor Donald,Lippai Lajos,Kószó Ferenc,Kiss Károly,Pópity Dániel"];
tablok[16] = ["Déri Miksa Ipari Szakközépiskola",1978,1982,"1982. össz. 001_0284.jpg",152,500,1];
nevek[16] = "Sores György,Disztl János,Csikós Csaba,Tóth Tamás,Farkas Zsolt,Katona Károly,Trója János,Patoszkai József,Vér József,Kernya István,Balogh Róbert,Süli János,Magyar Pál,Szőke Imre,Lehotai Ferenc,Lédeczi Bertalan,Szente István,Lázár Sándor,Balogh György,Fenyős Árpád,Rauf Csaba,Roszkos János,Dékány Ferenc,Bogár Pál,Dobó István,Pocsai Zoltán,Hajagos Sándor,Dicső Zoltán,Csjernyik András,Kozó László,Hódi Tamás,Bakó Tamás,Dékány Tibor,Masa Zoltán,Mucsi István,Németh István,Angelo Gábor,Péter József,Grósz Ferenc,Gábor Zoltán,Kovács Ferenc,Malustyik Mihály,Virág Tibor,Tóth Attila,Poóy István,Kalla Gábor,Gémesi Sándor,Novák Mihály,Varga Attila,Gyenes Kálmán,Szilágyi György,Kónya József,Juhász Árpád,Vén Zoltán,Sárosi Róbert,Herédi Attila,Gyenes Kálmán,Szilágyi György,Kónya József,Juhász Árpád,Vén Zoltán,Sárosi Róbert,Herédi Attila,Szabó Károly,Dávid Sándor,Gombos János,Kovács Ferenc,Tumbász János,Schadt László,Markó Mihály,Nagy Csaba,Brand Zoltán,Valek Endre,Makra Sándor,Horváth Csaba,Csiszár Zsolt,Hegedűs István,Árokszálási György,Czékus Géza,Fekete László,Darázs Zoltán,Ézsiás Tamás,Czernák Zoltán,Karkus Gyula,Bráda Csaba,Ötvös Sándor,Pogonyi Zoltán,Sánta Attila,Szabó Tibor,Zsolnasi András,Tóth József,Csatlós László,Balogh Lajos,Kómár Kornél,Fábiaán István,Szarouveiz Gáspár,Ambrus Imre,Bálint Bata Géza,Bioskei Tibor,Bohner László,Császár István,Csinos József,Fazekas Zoltán,Gelányi Zoltán,Méhes György,Gilicz László,Horváth Miklós,Jávorkai György,Kis István,Kovács Mátyás,Kocsis Ferenc,Schreither Zoltán,Madár Iván,Réti Attila,Pásztor András,Patyi Péter,Petróczi Béla,Raposány Árpád,Rácz Miklós,Rétfalvi Róbert,Pálvölgyi Gábor,Muosi Gábor,Szabó Antal,Szabó László,Tárkány Zsolt,Tóth Molnár Géza,Vnjkov József,Sziravicza Ernő";
tablok[17] = ["Déri Miksa Gépipari Technikum",1967,1971,"1971. össz.  001_0280.jpg",130,500,1];
nevek[17] = "Bárdos János,Ács Zoltán,Hrabovszki Mihály,Tóth Imre,Kékes János,Schlosser Miklós,Kollár János,Szabó Barna,Dér Győző,Farkas István,Ancsa Mihály,Rosta Sándor,Tömösi Károly,Nagy László,Szatmári Ferenc,Halmi Imre,Túri István,Molnár Imre,Szabó Ferenc,Ács Sándor,Kocsis László,Heraszika János,Zubány Csaba,Veréb János,Rózsa Péter,Sándor Gyula,Frajka József,Ilia Pál,Sirbka János,Varga József,Sántha József,Horváth Sándor,Hurtony István,Rostás Imre,Ungvári Sándor,Kiss János,Kalcsó József,Malitorisz Lajos,Czene Péter,Ragány Gyula,Csuvár József,Kiss Zoltán,Karcagi Pál,Zsigmond István,Bokor Árpád,Bíró József,Tóth György,Rácz Pál,Protity Miklós,Bányai László,Rácz István,Csányi György,Sárszki András,Kiss József,Solti Antal,Major Sándor,Szőcs Mihály,Cséfán Lajos,Bakacsi Tamás,Rácz Imre,Zsamoczay László,Beregszászi Zoltán,Farkas Zoltán,Matuska Zoltán,Ábrahám Zsolt,LázárDezső,Csáki István,Avramucz István,Lévai Imre,Besztercei János,Haskó Béla,Sáringer Károly,Kacsó Pál,Kósa Árpád,Husbanov Mihály,Nyerges László,Vass Ferenc,Lele Gyula,Csikos András,Pádi László,Rácz Imre,Fróman Gábor,Joanovics László,Tóth Endre,Farkas Zoltán,Sárosi Vilmos,Dóró Kovács János,Papp Lajos,Róvó István,Varga László,Fischóf József,Radnai Mihály,Bakaity József,Valentovics István,Falusi János,Belányi Gyula,Kádár Péter,Szatmári István,Ábrahám Ferenc,Varga János,Maté Tamás,Földesi Gyula,Imre László,Gazsó Imre,Szántó Tibor,Hörömpő Erika,Mucsi Mária,Matuz Olga,Domonkos Erzsébet,Haracsi Margit,Rózsa István,Molnár Zoltán,Fodor István,Mihály Gábor,Mihály Gyula,Fodor Pál,Molnár Pál,Nagyiván Péter,Rickert Benedek,Farkas András,Jójárt György,Bozóki Csaba,Török Sándor,Rózsa Sándor,Antal József,Asztalos Imre,Fehér Zoltán,Fohsz Béla,Kiss László,Szabuka Imre,Czrják József,Fontos Zoltán,Csizmadia Béla,Hidasi Bálint";
tablok[18] = ["Déri Miksa Gépipari Technikum",1950,1954,"1954. esti vill.  001_0277.jpg",116,500,1];
nevek[18] = "Csóti József,Peták József,Hegedűs Pál,Kozma Gyula,Csaba Lajos,Németh Zoltán,Fülöp Sándor,Kovács János";
tablok[19] = ["SZSZC Déri Miksa Szakgimnáziuma Szakközépiskolája",2014,2018,"2018.A Gergelynétől.jpg",98,500,1];
nevek[19] = "Nagy Martin,Szabó-Bozsó Attila,Kádár Tibor,Vecsernyés Roland,Molnár Tamás,Fődi Tamás,Kocsispéter Viktor,Majer Attila,Trombitás Imre,Kucska Krisztián,Gérgény Ádám,Tímár Mátyás,Miklós Roland,Babecz Kristóf,Veszelovszki Csaba,Kis Gellért,Kun-Szabó Norman,Berkó Tamás,Bodics Dávid,Ördögh Zsolt,Nagy Tamás,Karancevic Dávid,Papp Martin,Kalla Lajos Péter,Szélpál Attila,Kardos András,Tálas Dávid,Sebők Ramón";
tablok[20] = ["Gépipari Technikum Villamostagozata",1952,1956,"1952.vill.   001_0167.jpg",103,482,1];
nevek[20] = "Pálinkás Gyula,Tamás János,Krajcsi Károly,Szegvári József,Bujdosó Mihály,Lőrincky Lajos,Kósa László,Czirle András,Zsebik Pál,Rechnitzer Edgár,Palócz Lajos,Vidákovics József,Fövényi Gyula,Turdéli József,Rostás István";
tablok[21] = ["Dolgozók Gépipari Technikum Gépésztagozat",1950,1954,"1954.G esti    001_0267.jpg",105,469,1];
nevek[21] = "Farkas László,Reisz János,Kátai István,Rónyai László,Domonkos András,Farkas Szilárd,Deák Vilmos,Ungi István,Rózsa János,Nagy Sándor,Dr. Fekete Márta,Gelei Tibor,Szecskár Mihály,Lajkó Ágoston,Mattyasovszki László,Jenei István,Balogh István,Kószó Ferenc,Hajdú Pál";
tablok[22] = ["Déri Miksa Gépipari Technikum",1971,1971,"1971.G esti   001_0266.jpg",106,454,1];
nevek[22] = "Bauer Tibor,Hamzsek Sándor,Kalapos Jószef,Imre István,Halász Ibolya,Boldizsár Mihály,Prágai György,Felső Pál,Dobó József,Metzger István,Ficsor Sándor,Nyerges József,Farkas János,Bónus Péter,Pintér László,Ökrös József,Szabó József,Kiss András,Bori Zoltán,Sebesvári Mihály,Tanács István,Nagy Sándor,Domonkos István,Frányó Antal,Nagy Ferenc,Pogonyi László,Batki János,Ecseki Árpád,Herédi István,Boros József,Hügl Mihály,Zsigmond Zoltán,Michel Rezső,Sipos Tibor,Börcsök József,Balogh Antal,Mikula Ferenc,Kocsmár Attila,Dohány Mihály,Jenei Sándor";
tablok[23] = ["Déri Miksa Gépipari Technikum",1952,1956,"1956.D  001_0263.jpg",110,435,1];
nevek[23] = "Frank Norbert,Kiss Károly,Varga Dezső,Takács Ferenc,Szabó Erzsébet,Meggyesi Katalin,Uhrin Mihály,Kristóf András,Sipos István,Paksi Róbert,Csiszár István,Tulipán István,Bodrogi István,Regdon Kálmán,Kotormán Dániel,Mihály István,Vörös József,Bárdos Imre,Karona József,Szabó Ferenc,Sajti János,Mohácsi László,Petrók Károly,Vajdovich Béla,Vigh Zoltán,Németh Tibor";
tablok[24] = ["Déri Miksa Ipari Szakközépiskola",2001,2005,"2005.C   001_0262.jpg",65,481,1];
nevek[24] = "Kósa Dávid,Köles Róbert,Fehér Zoltán,Sipos Ádám Tamás,Kovács Dávid,Benkő Balázs,Csaba Csaba,Papp Norbert,Nagy Tamás,Lovai Tamás,Hebők László,Fekete Gergő,Lázár Éva,Hegedűs Péter,Szabó Attila,Magyar Béla,Molnár Zoltán,Demus László Andor,Papp Levente,Bognár Szabolcs,Rácz József,Erdődi Ferenc,Mészáros Alexandra Éva,Masa Attila,Császár István,"
tablok[25] = ["Déri Miksa Ipari Szakközépiskola Elektronika-Elektrotechnika",2004,2008,"2008.D  001_0257.jpg",56,470,1];
nevek[25] = "Kuczora Péter,Debreczeni Ádám,Csányi Péter,Kalmár Sándor,Ferenczi András,Szabó Sándor,Csamangó Dávid,Korbel Zoltán,Dobó Zsombor,Zsila Gábor,Pamuk Attila,Szin CSaba,Kocsis László,Nagy Tamás,Polák Antal,Farkas Dániel,Tóth Róbert,Juhász Gábor,Bús Péter,Kele Péter,Bóka Tamás,Igaz Bálint,Fehér Róbert";
tablok[26] = ["Déri Miksa Gépipari Technikum Levelező Tagozat",1968,1972,"1972.lev.  001_0255.jpg",57,457,1];
tablok[27] = ["Déri Miksa Általános Gépipari Technikum Levelező Tagozat",1964,1968,"1968.lev. 001_1051.jpg",59,444,1];
tablok[28] = ["Déri Miksa Gépipari Technikum",1968,1968,"1968.esti   001_0253.jpg",71,444,1];
tablok[29] = ["Déri Miksa Ipari Szakközépiskola",2004,2008,"2008.A  001_0249.jpg",62,425,1];


tablok_osszes = tablok_osszes.concat(tablok);