﻿/* csoport5.js
 *
 * tablók listája
 * épület szintje: -1
 * készítették: Kun-Szabó Zoltán, Zatykó Levente
 */
 
 /*
    fájl meghatározásánál ügyeljenek a szóközökre!
    
    X,Y pozíciók meghatározásánál teljes képernyő, console alulra dokkolva,
    és próbálják ki, hogy mit ír ki a konzol egérkattintásra:
      a bal felső sarok kb. 0,0
      a jobb alsó sarok kb. 800,600
    --> csak ekkor lesz jó a konzolból leolvasott érték!
    
    nevek írásánál minden egyes tablok elem alatt párhuzamosan legyen egy tömbelem a neveknek. pl.:
    tablok[12] = [...];
    nevek[12] = "Alfa Áron, Béta Barnabás, Gamma Gábor";
 */
 
 var tablok = [];
 //		[tabló neve, mikortól, mikorig, fájl neve, x, y, szint]
  tablok[0] = ["Déri Miksa Ipari Szakközépiskola 12.A", 1986, 1990, "1990.A  001_1097.jpg", 212, 500, -1];
	tablok[1] = ["Déri Miksa Gépipari Szakközépiskola 12.A", 1989, 1993, "1993.A 001_1359.jpg", 315, 570, -1];
	tablok[2] = ["Déri Miksa Ipari Szakközépiskola 12.D", 1989, 1993, "1993.D 001_1355.jpg", 370, 570, -1];
	tablok[3] = ["Szegedi Ipari Szakképző és Általános Iskola Déri Miksa Tagintézménye 12.B", 2011, 2015, "2015.B 001_1353.jpg", 370, 560, -1];
	tablok[4] = ["Déri Miksa Ipari Szakközépiskola 12.D", 1989, 1990, "1990.D  001_1351.jpg", 370, 550, -1];
	
 // bemásoljuk a tablok_osszes táblába
 tablok_osszes = tablok_osszes.concat(tablok);