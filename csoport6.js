﻿//6-os "csoport"
//épület szintje: 2
//készítette: Nagy Dominik

 /*
    fájl meghatározásánál ügyeljenek a szóközökre!
    
    X,Y pozíciók meghatározásánál teljes képernyő, console alulra dokkolva,
    és próbálják ki, hogy mit ír ki a konzol egérkattintásra:
      a bal felső sarok kb. 0,0
      a jobb alsó sarok kb. 800,600
    --> csak ekkor lesz jó a konzolból leolvasott érték!
    
    nevek írásánál minden egyes tablok elem alatt párhuzamosan legyen egy tömbelem a neveknek. pl.:
    tablok[12] = [...];
    nevek[12] = "Alfa Áron, Béta Barnabás, Gamma Gábor";
 */
 
//          [tabló neve, mikortól, mikorig, fájl neve, x, y, szint]
var tablok1 = [];
tablok1[0] = ["Déri Miksa Gépipari Technikum esti tagozat", 1955, 1959, "1959.esti 001_0246.jpg", 480, 505, 2];
tablok1[1] = ["17.Gépipari Technikum villamos tagozatának végzett hallgatói", 1950, 1954, "1954.Vill. 001_0243.jpg", 470, 505, 2];
tablok1[2] = ["Déri Miksa Gépipari Technikum esti tagozat", 1963, 1967, "1967.esti 001_0241.jpg", 460, 505, 2];
tablok1[3] = ["Déri Miksa Ipari Szakközépiskola elektronika tagozat 12.d", 2001, 2005, "2005.D 001_0235.jpg", 435, 470, 2];
tablok1[4] = ["Déri Miksa Ipari Szakközépiskola 12.d villamosgép és készülék szerelő szak", 1997, 2001, "2001.D 001_0233.jpg", 415, 470, 2];
tablok1[5] = ["Déri Miksa Ipari Szakközépiskola mechatronika 12.b", 1996, 2000, "2000.B 001_0230.jpg", 405, 470, 2];
tablok1[6] = ["Déri Miksa Ipari Szakközépiskola elektronikai műszerész 12.e", 1997, 2001, "2001.E 001_0228.jpg", 390, 470, 2];
tablok1[7] = ["SZISZSZI Déri Miksa Tagintézménye informatika szakmacsoport 12.b", 2010, 2014, "2014.B  001_0225.jpg", 360, 470, 2];
tablok1[8] = ["Gépipari Technikum 1949-53ban végzett hallgatói", 1949, 1953, "1953.A 001_0221", 340, 470, 2];
tablok1[9] = ["17.sz Gépipari Technikum IV.d", 1951, 1955, "1955.D 001_0149.jpg", 300, 470, 2];
tablok1[10] = ["A Szegedi M. Áll. Felsőipariskola végzett gépész hallgatói", 1944, 1947, "1947. G gépész 001_0217.jpg", 390, 470, 2];
tablok1[11] = ["Déri Miksa Szakközépiskola esti tagozat", 1971, 1975, "1975.esti 001_0215.jpg", 270, 470, 2];
tablok1[12] = ["Gépész-tagozat IV.a IV.b, Villamos Tagozat IV.c IV.d", 1965, 1969, "1969. össz.001_0213.jpg", 250, 480, 2];
tablok1[13] = ["Déri Miksa Ipari Szakközépiskola 12.b Informatika", 2004, 2008, "2008.B 001_0204.jpg", 250, 495, 2];
tablok1[14] = ["Szegedi 17.sz Gépipari Techikum", 1950, 1954, "1954.C 001_0199.jpg", 235, 505, 2];
tablok1[15] = ["Szegedi Állami Felsőipariskola gépészeti tagozatának végzett hallgatói", 1946, 1949, "1949.gépsz 001_0197.jpg", 225, 505, 2];
tablok1[16] = ["Szegedi Állami Gépipari Műszaki Középiskola végzett hallgatói", 1947, 1951, "1951.A 001_0195.jpg", 215, 505, 2];
tablok1[17] = ["Szegedi Áll. Felsőipariskola villamosipari tagozatának 1946-1949-ben végzett növendékei", 1946, 1949, "1949. vill. 001_0183.jpg", 130, 505, 2];
tablok1[18] = ["Szegedi Állami Felsoipariskola villamos tagozatának végzett hallgatói", 1944, 1947, "1947. E vill. 001_0181.jpg", 110, 505, 2];
tablok1[19] = ["Szegedi Gépipari Technikum erőmű gépészeti tagozatának IV.c-ben végzett hallgatói", 1951, 1955, "1955.C 001_0219.jpg", 100, 480, 2];
tablok1[20] = ["Szegedi Gépipari Technikum villamos tagozatának végzett hallgatói", 1948, 1952, "1952.vill. 001_0167.jpg", 107, 460, 2];
tablok1[21] = ["Dolgozók Ipari Középiskolája Szeged. Villamos tagozat első érettségizők", 1946, 1949, "1949.dolg. 001_0165.jpg", 111, 440, 2];
tablok1[22] = ["Déri Miksa Gépipari Technikum levelező tagozat", 1965, 1969, "1969.lev. 001_0238.jpg", 460, 530, 2];
tablok1[23] = ["Déri Miksa Informatika", 2005, 2009, "2009.C 001_0223.jpg", 290, 530, 2];
tablok1[24] = ["A szegedi Déri Miksa Gépipari Technikum elektromos tagozatának 1954-58ban végzett hallgatói", 1954, 1958, "1958.C elektr.jpg", 240, 530, 2];
tablok1[25] = ["Déri Miksa Gépirai Szakközépiskola esti tagozat", 1974, 1978, "1978.esti 001_0189.jpg", 220, 530, 2];
tablok1[26] = ["Szegedi Ipari Középiskola Déri Miksa Tagintézménye 12.d", 2006, 2010, "2010.D 001_0192.jpg", 220, 540, 2];
tablok1[27] = ["Déri Miksa Gépipari Technikum esti tagozat", 1965, 1969, "1969.esti 001_0185.jpg", 170, 530, 2];
tablok1[28] = ["Déri Miksa Ipari Szakközépiskola 12.a", 1998, 2002, "2002.A 001_0187.jpg", 170, 540, 2];
tablok1[29] = ["Gépészek", 1944, 1948, "1948.gépész 001_0177.jpg", 130, 530, 2];
tablok1[30] = ["Szegedi Villamosipari Technikum 1953ban végzett hallgatói", 1949, 1953, "1953.vill 001_0176.jpg", 90, 530, 2];
tablok1[31] = ["A Déri Miksa Ipari Szakközépiskola Technikus Évfolyama", 1979, 1984, "1984. NAPPALI Technikusi évfolyam 001_0173.jpg", 70, 530, 2];
tablok1[32] = ["Szegedi Állami Felsőipariskola elektromos tagozatának 1948ban végzett növendékei", 1944, 1948, "1948. vill. 001_0163.jpg", 70, 515, 2];
tablok1[33] = ["Dolgozók Ipari Középiskolája Szeged gépipari tagozat első érettségizők", 1946, 1949, "1949.dolg. 001_0159.jpg", 70, 500, 2];
tablok1[34] = ["Déri Miksa Ipari Szakközápiskola 12.d", 1994, 1998, "1998.D 001_0161.jpg", 60, 500, 2];
tablok1[35] = ["Szegedi Gépipari Technikum villamos tagozatának végzett növendékei", 1951, 1955, "1955.E vill. 001_0157.jpg", 70, 480, 2];
tablok1[36] = ["A szegedi Műszaki Középiskola gépészeti tagozatának végzett hallgatói", 1947, 1951, "1951.B gépész 001_0155.jpg", 70, 460, 2];
tablok1[37] = ["szegedi Villamosipari Műszaki Középiskola 1947-1951 végzett tanulói", 1947, 1951, "1951.vill 001_0147.jpg", 80, 420, 2];

tablok_osszes = tablok_osszes.concat(tablok1);