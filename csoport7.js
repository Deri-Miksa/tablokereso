﻿//csoport7.js
//épület szintje: földszint
//készítették: Csatlós Zoltán, Kispál Róbert 

 /*
    fájl meghatározásánál ügyeljenek a szóközökre!
    
    X,Y pozíciók meghatározásánál teljes képernyő, console alulra dokkolva,
    és próbálják ki, hogy mit ír ki a konzol egérkattintásra:
      a bal felső sarok kb. 0,0
      a jobb alsó sarok kb. 800,600
    --> csak ekkor lesz jó a konzolból leolvasott érték!
    
    nevek írásánál minden egyes tablok elem alatt párhuzamosan legyen egy tömbelem a neveknek. pl.:
    tablok[12] = [...];
    nevek[12] = "Alfa Áron, Béta Barnabás, Gamma Gábor";
 */
 
var tablok7 = [];

tablok7[0] = ["Déri Miksa Ipari Szakközépiskola 12.ABCD", 1980, 1984, "1984.  össz.  001_1029", 480, 528, 0];
tablok7[1] = ["Déri Miksa Ipari Szakközépiskola Villamosgép és Készülék Szerelő Szak 12.D", 1996, 2000, "2000.D  001_1033", 510, 528, 0];
tablok7[2] = ["Déri Miksa Erősáramú Villamos Levelező Tagozat", 1981, 1985, "1985.lev. vill.  001_1031", 510, 528, 0];
tablok7[3] = ["Szegedi Ipari Középiskola Déri Miksa Tagintézménye 12.A", 2007, 2011, "2011. A  001_1039", 540, 528, 0];
tablok7[4] = ["Déri Miksa Ipari Szakközépiskola Elektronikai Műszerész Szak 12.E", 1996, 2000, "2000.E  001_1037", 540, 528, 0];
tablok7[5] = ["Déri Miksa Ipari Szakközépiskola 12.ABCD", 1981, 1985, "1985.  össz.  001_1042", 570, 528, 0];
tablok7[6] = ["Szegedi Ipari Szakképző és Általános Iskola Déri Miksa Tagintézménye 12.D", 2011, 2015, "2015.D  001_1045", 600, 528, 0];
tablok7[7] = ["Déri Miksa Ipari Szakközépiskola 12.A", 2001, 2005, "2005.A   001_1047.jpg", 630, 528, 0];
tablok7[8] = ["Déri Miksa Általános Gépipari Technikum Levelező Tagozata", 1964, 1968, "1968.lev. 001_1051", 630, 528, 0];
tablok7[9] = ["Szegedi Ipari Középiskola Déri Miksa Tagintézménye 12.B", 2008, 2012, "2012.B  001_1088", 710, 528, 0];
tablok7[10] = ["Déri Miksa Általános Gépipari Technikum Gépész Levelező Tagozat", 1962, 1966, "1966.lev. gépész  001_1086", 710, 528, 0];
tablok7[11] = ["Déri Miksa Ipari Szakközépiskola 12.E", 1990, 1994, "1994.E 001_0995", 245, 490, 0];
tablok7[12] = ["Déri Miksa Ipari Szakközépiskola 12.C", 1988, 1992, "1992.C  001_1002", 280, 474, 0];
tablok7[13] = ["Szegedi Ipari, Szolgáltató Szakképző és Általános Iskola Déri Miksa Tagintézménye 12.A", 2009, 2013, "2013.A  001_1005", 280, 474, 0];
tablok7[14] = ["Déri Miksa Ipari Szakközépiskola 12.E", 1994, 1998, "1998.E  001_0379", 300, 474, 0];
tablok7[15] = ["Déri Miksa Ipari Szakközépiskola IV.B", 1989, 1993, "1993.B  001_1014.jpg", 340, 474, 0];
tablok7[16] = ["Déri Miksa Ipari Szakközépiskola", 1987, 1991, "1991. össz.  001_1019.jpg", 417, 474, 0];
tablok7[17] = ["Déri Miksa Ipari Szakközépiskola IV.D", 1988, 1992, "1992.D  001_1025.jpg", 441, 487, 0];
tablok7[18] = ["Déri Miksa Ipari Szakközépiskola", 1982, 1986, "1986.  össz.    001_5014 .jpg", 460, 503, 0];
tablok7[19] = ["Déri Miksa Ipari Szakközépiskola", 1983, 1987, "1987.  össz.    001_5008.jpg", 470, 503, 0];
tablok7[20] = ["Szegedi Ipari Középiskola Déri Miksa Tagintézménye Elektronika 12.d", 2007, 2011, "2011.D  001_5005.jpg", 490, 503, 0];
tablok7[21] = ["Déri Miksa Ipari Szakközépiskola Világbanki Informatika 12/b", 2000, 2004, "2004.B   001_5003.jpg", 490, 503, 0];
tablok7[22] = ["Déri Miksa Ipari Szakközépiskola", 1985, 1989, "1989.  össz. 001_4994.jpg", 530, 503, 0];
tablok7[23] = ["Déri Miksa Mûszaki Szakközépiskola Technikus Osztály", 1988, 1989, "1989.Techn.   001_4989.jpg", 540, 503, 0];
tablok7[24] = ["Déri Miksa Ipari Szakközépiskola", 1984, 1988, "1988.  össz.    001_4987.jpg", 550, 503, 0];
tablok7[25] = ["Szegedi Déri Miksa Gépipari Technikum Esti Tagozatának Végzett Hallgatói", 1954, 1958, "1958.esti 001_1064.jpg", 606, 495, 0];
tablok7[26] = ["Déri Miksa Ipari Szakközépiskola Nappali Technikusi Tagozata", 1984, 1985, "1985.techn. 001_1060.jpg", 606, 485, 0];
tablok7[27] = ["Szegedi Gépipari Technikum Végzett Hallgatói", 1954, 1958, "1958.A   001_1058.jpg", 615, 475, 0];
tablok7[28] = ["Gépészeti Technikum Végzett Hallgatói", 1949, 1953, "1953.B   001_1067.jpg", 622, 485, 0];
tablok7[29] = ["Déri Miksa Gépipari Technikum Gépész Levelezõ Tagozata", 1963, 1967, "1967.lev. gépész  001_1071.jpg", 622, 495, 0];
tablok7[30] = ["Déri Miksa Ipari Szakközépiskola 12.a", 1963, 1967, "2007.A  001_1075.jpg", 622, 495, 0];
tablok7[31] = ["Déri Miksa Gépipari Technikum Végzett Hallgatói", 1956, 1960, "1960.  össz. 001_1082.jpg", 622, 495, 0];

tablok_osszes= tablok_osszes.concat(tablok7);