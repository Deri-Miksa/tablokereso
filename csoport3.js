﻿/*
épület szintje:1.emelet
Készítették: Magyar Máté, Komjáti Gergő
*/

 /*
    fájl meghatározásánál ügyeljenek a szóközökre!
    
    X,Y pozíciók meghatározásánál teljes képernyő, console alulra dokkolva,
    és próbálják ki, hogy mit ír ki a konzol egérkattintásra:
      a bal felső sarok kb. 0,0
      a jobb alsó sarok kb. 800,600
    --> csak ekkor lesz jó a konzolból leolvasott érték!
    
    nevek írásánál minden egyes tablok elem alatt párhuzamosan legyen egy tömbelem a neveknek. pl.:
    tablok[12] = [...];
    nevek[12] = "Alfa Áron, Béta Barnabás, Gamma Gábor";
 */

  var tablok1 = [];
  //          [tabló neve, mikortól, mikorig, fájl neve, x, y]
	tablok1[0] = ["12.A", 2009, 2013, "2013.A  001_1005.jpg", 280, 480, 1];
	tablok1[1] = ["12.C", 1988, 1992, "1992.C  001_1002.jpg", 280, 470, 1];
	tablok1[2] = ["12.E", 1990, 1994, "1994.E 001_0995.jpg", 250, 490, 1];
	tablok1[3] = ["Villamosgép és készülékszerelő 12.D", 1995, 1999, "1999.D  001_0982.jpg", 240, 525, 1];
	tablok1[4] = ["12.E", 1992, 1996, "1996.E   001_0977.jpg", 240, 535, 1];
	tablok1[5] = ["12.B", 1991, 1995, "1995.B  001_0988.jpg", 230, 505, 1];
	tablok1[6] = ["12.E", 1991, 1995, "1995.E  001_0986.jpg", 215, 505, 1];
	tablok1[7] = ["Gépgyártástechnológia-számítástechnika 12.A", 1991, 1995, "1995.A 001_0984.jpg", 200, 505, 1];
	tablok1[8] = ["12.B", 1990, 1994, "1994.B  001_0973.jpg", 200, 525, 1];
	tablok1[9] = ["Levelező tagozat", 1988, 1992, "1992.lev.  001_0970.jpg", 200, 535, 1];
	tablok1[10] = ["12.D", 1990, 1994, "1992.lev.  001_0970.jpg", 160, 525, 1];
	tablok1[11] = ["12.C", 1989, 1993, "1993.C  001_0963.jpg", 160, 535, 1];
	tablok1[12] = ["Informatika 12.B", 2007, 2011, "2011.B  001_0959.jpg", 160, 505, 1];
	tablok1[13] = ["Informatika 12.B", 2012, 2016, "2016.B   Blatt Attilától.jpg", 145, 505, 1];
	tablok1[14] = ["Gépipari technikum levelező tagozat", 1960, 1964, "1964.lev. 001_0957.jpg", 100, 480, 1];
	tablok1[15] = ["Erősáramú villamosenergia levező tagozat", 1975, 1979, "1979.lev. vill.   001_0955.jpg", 70, 480, 1];
	tablok1[16] = ["12.A", 1990, 1994, "1994.A  001_0953", 75, 460, 1];

	tablok_osszes=tablok_osszes.concat(tablok1);