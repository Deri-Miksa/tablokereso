﻿//csoport2.js

//Készítették: Gyalai Evelin, Grimshaw Kristóf, Hamar Imre 
//Épület szintje: 1. emelet

 /*
    fájl meghatározásánál ügyeljenek a szóközökre!
    
    X,Y pozíciók meghatározásánál teljes képernyő, console alulra dokkolva,
    és próbálják ki, hogy mit ír ki a konzol egérkattintásra:
      a bal felső sarok kb. 0,0
      a jobb alsó sarok kb. 800,600
    --> csak ekkor lesz jó a konzolból leolvasott érték!
    
    nevek írásánál minden egyes tablok elem alatt párhuzamosan legyen egy tömbelem a neveknek. pl.:
    tablok[12] = [...];
    nevek[12] = "Alfa Áron, Béta Barnabás, Gamma Gábor";
 */

//          [tabló neve, mikortól, mikorig, fájl neve, x, y]
		var tablok1 = [];
		tablok1[0] = ["Déri Miksa Gépipari Technikum IV.ABC", 1960, 1964, "1960.  össz. 001_1082.jpg", 654, 791,1];
       tablok1[1] = ["Déri Miksa Gépipari Technikum IV.ABC", 1961, 1965, "1965. össz. 001_0323.jpg", 359,687,1];
       tablok1[2] = ["Déri Miksa Szakközépiskolája IV.B", 1992, 1996, "1996.B  001_0330.jpg", 373,695,1];
       tablok1[3] = ["Déri Miksa Gépipari Technikum IV.ABCD", 1964, 1968, "1968. össz.  001_0364.jpg", 382,731,1];
       tablok1[4] = ["Déri Miksa Ált. Gép-Erősáramú Ipari Technikum IV.ABCD", 1968, 1972, "1972. össz.   001_0335.jpg", 384,711,1];
       tablok1[5] = ["Déri Miksa Szakközépiskola IV.ABCD", 1971, 1975, "1975. össz. 001_0337.jpg", 399,709,1]; 
       tablok1[6] = ["Déri Miksa Erősáramú Villamos-energiaipari Szakközépiskola Levelező Tagozat ", 1974, 1978, "1978.lev.  001_0360.jpg", 397,731,1];//ugyanaz a koordináta
       tablok1[7] = ["Déri Miksa Gépipari Technikum IV.A", 1955, 1959, "1959.A  001_0357.jpg", 397,731,1];//ugyanaz a koordináta
       tablok1[8] = ["Déri Miksa Szakközépiskola 12.B", 1999, 2003, "2003.B  001_0354.jpg", 409,730,1];//ugyanaz a koordináta
       tablok1[9] = ["Szegedi Gépipari Technikum Erőgépészeti Tagozat IV.B ", 1950, 1954, "1954.B  001_0351.jpg", 409,730,1];//ugyanaz a koordináta
       tablok1[10] = ["Déri Miksa Gépipari Technikum Esti Tagozat", 1969, 1973, "1973.esti  001_0339.jpg", 429,710,1];
       tablok1[11] = ["Déri Miksa Gépipari Szakközépiskola IV.ABCD", 1972, 1976, "1976. össz.  001_0341.jpg", 456,711,1];
       tablok1[12] = ["Déri Miksa Gépipari Szakközépiskola IV.ABCD", 1969, 1973, "1973. össz.  001_0345.jpg", 469,709,1];
       tablok1[13] = ["Déri Miksa Gépészeti és Erősáramú Szakközépiskola IV.ABCD", 1974, 1979, "1974. össz.  001_0349.jpg", 466,730,1];
       tablok1[14] = ["Déri Miksa Gépipari Technikum Hallgatói", 1958, 1962, "1962. össz.  001_0366.jpg", 493,731,1];
       tablok1[15] = ["Déri Miksa Gépipari Technikum IV.B", 1957, 1961, "1961.B  001_0369.jpg", 464,701,1];
       tablok1[16] = ["Déri Miksa Ipari Szakközépiskola 12.B", 1995, 1999, "1999.B  001_0398.jpg", 474,679,1];
       tablok1[17] = ["Szegedi Déri Miksa Gépipari Technikum  IV.A ", 1957, 1961, "1961.A  001_0395.jpg", 477,667,1];//ugyanaz a koordináta
       tablok1[18] = ["Déri Miksa Ipari Szakközépiskola 12.D", 1991, 1995, "1995.D  001_0393.jpg", 477,667,1];//ugyanaz a koordináta
       tablok1[19] = ["Déri Miksa Általános Gépipari Technikum Levelező Tagozat", 1969, 1973, "1973.lev.  001_0391.jpg", 504,668,1];
       tablok1[20] = ["Déri Miksa Ipari Szakközépiskola IV.ABCD ", 1979, 1983, "1983.  össz.   001_0387.jpg", 504,668,1];
       tablok1[21] = ["Déri Miksa Ipari Szakközépiskola 12.C ", 2002, 2006, "2006.C  001_0383.jpg", 508,679,1];
       tablok1[22] = ["Déri Miksa Ipari Szakközépiskola Elektronika Műszerész Szak 12.E ", 1995, 1999, "1999.E  001_0381.jpg", 519,684,1];
       tablok1[23] = ["Déri Miksa Ipari Szakközépiskola  12.E ", 1994, 1998, "1998.E  001_0379.jpg", 530,685];
       tablok1[24] = ["Déri Miksa Gépészeti Erősáramú Szakközépiskola IV.ABCD", 1973, 1977, "1977.  össz.  001_0377.jpg", 522,702,1];

//1.emelet lépcső
		tablok1[25] = [" Déri Miksa Általános Gépipari Technikum Esti Tagozat", 1968, 1972, "1972.esti    001_1325.jpg",288,711,1];
        tablok1[26] = [" Déri Miksa Általános Gépipari Technikum Gépész Esti Tagozat", 1962, 1966, "1966.esti gépész   001_1327.jpg",288,720,1];
        tablok1[27] = [" Déri Miksa Ipari Szakközépiskola 12.C", 1998, 2002, "2002.C  001_1332.jpg",288,734,1];
        tablok1[28] = [" DMGSZ Gépészet levelező tagozat", 1979, 1983, "1983.lev. gépész  001_1330.jpg",288,734,1];
        tablok1[29] = [" Déri Miksa Ipari Szakközépiskola Gépész Levelező Tagozat", 1981, 1985, "1985.lev. gépész  001_1335.jpg",245,737,1];
        tablok1[30] = [" Déri Miksa Ipari Szakközépiskola 12.ABCD", 1976, 1980, "1980.  össz. 001_1336.jpg",248,737,1];//koordináta hiba javítva
        tablok1[31] = [" Déri Miksa Gépészeti és Erősáramú Szakközépiskola 12.ABCD", 1975, 1979, "1979.  össz.  001_1341.jpg",244,729,1];
        tablok1[32] = [" Déri Miksa Gépészeti és Erősáramú Szakközépiskola 12.ABCD", 1977, 1981,"1981. ösz.  001_1344.jpg",245,724,1 ];
        tablok1[33] = [" Déri Miksa Gépipari Szakközépiskola Gépész Tagozat 12.E", 1976, 1980, "1980.esti gépész 001_1348.jpg",246,712,1];

    //2.emelet lépcső
		tablok1[34] = ["Déri Miksa Gépészeti és erősáramú szakközépiskola 12.ABCD", 1974, 1978, "1974. össz.  001_0349.jpg",  318, 549,2 ];
        tablok1[35] = ["17. számú Gépipari Technikum 12.B", 1952, 1956, "1956.D  001_0263.jpg",319, 565,2];
        tablok1[36] = ["Gépipari technikum IV.D Villamos osztály 12.D", 1974, 1978, "1957.D  001_1295.jpg",319,579,2];
        tablok1[37] = ["Déri Miksa Szakközépiskola elektronika, elektrontechnika szak 12.D", 1998,2002, "2002.D  001_1301.jpg",310,579,2 ];
        tablok1[38] = ["Szegedi Gépipari Technikum", 1957, 1961, "1961.C  2001-ben  001_1313.jpg" ,310,740,2 ];
        tablok1[39] = ["Gépipari Technikum IV.B", 1955,1959, "1959.B  001_1303.jpg",310, 760, 2];
        tablok1[40] = ["Gépipari Technikum", 1957, 1961, "1961.C  001_1304.jpg",306, 752,2];
        tablok1[41] = ["Déri Miksa Ipari Szakközépiskola Informatika és elektronika", 1999, 2003, "2003.C  001_1309.jpg",307, 755,2];
        tablok1[42] = ["Szegedi Ipari Szolgáltató Szakképző és Általános Iskola Déri Miksa Tagintézménye", 2009, 2013, "2013.D  001_1316.jpg",305, 761,2];
        tablok1[43] = [" Szegedi Ipari Szakképző és Általános Iskola Déri Miksa Tagintézménye", 1999, 2003, "2014.A  001_1321.jpg",305, 767,2]	; 

//1.emelet cad-eknél lévő  lépcső
        tablok1[44] = [" Déri Miksa Ipari Szakközépiskola Erősáramú Szak", 1986, 1990, "1990.D  001_1351.jpg",448,415 ,1];
        tablok1[45] = [" Szegedi Ipari Szakképző és Általános Iskola Déri Miksa Tagintézménye ", 2011, 2015, "2015.B  001_1353.jpg",459,416,1];
        tablok1[46] = [" Déri Miksa Ipari Szakközépiskola", 1989, 1993, "1993.D  001_1355.jpg",463,415,1 ];
        tablok1[47] = [" Déri Miksa Gépipari Szakközépiskola", 1989, 1993, "1993.A  001_1359.jpg",421,386,1 ];

    //II.emelet lépcső, ÚJ CAD
        tablok1[48] = [" Déri Miksa Ipari Szakközépiskola", 2003, 2007,"2007.D  001_1428.jpg" ,422,409 ,2];
        tablok1[49] = [" Déri Miksa Ipari Szakközépiskola", 2003, 2007,"2007.C  001_1430.jpg " ,444,387 ,2];
        tablok1[50] = [" Déri Miksa Ipari Szakközépiskola", 2002, 2006,"2006.A  001_1432.jpg" ,437,388,2];
        tablok1[51] = [" Déri Miksa Ipari Szakközépiskola", 1993, 1997,"1997.D  001_1437.jpg " ,458,387,2 ];
        tablok1[52] = [" Déri Miksa Gépipari Technikum", 1962, 1966, "1966. Össz. 001_1440.jpg",433,388 ,2];
	   
	   tablok_osszes= tablok_osszes.concat(tablok1);