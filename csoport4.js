﻿/*
csoport4.js
Tablók listája: 

Déri Miksa Ipari Szakközépiskola 12.D
Déri Miksa Gépipari Szakközépiskola Gépészeti Szak Levelező
Déri Miksa Ipari Szakközépiskola 4.C
Szegedi Ipari Középiskola Déri Miksa Tagintézménye 12.b
Szegedi Ipari Középiskola Déri Miksa Tagintézménye 12.c
Déri Miksa Ipari Szakközépiskola 4.c
Déri Miksa Ipari Szakközépiskola 12.d
Déri Miksa Gépipari Technikum 4.c
A Szegedi Gépipari Technikum 4.A
Déri Miksa Általános Gépipari Technikum Levelező Tagozat
Déri Miksa Ipari Szakközépiskola 12.A
Déri Miksa Gépipari Technikum Esti
Dólgozók Ipari Középiskolája Szeged Villamos Tagozatának Érettségizői
Gépipari Technikum Esti Villamos Tagozatának 
Szegedi Ipari Középiskola Déri Miksa Tagintézménye 12.A
Szegedi Gépipari Technikum 4.C
Déri Miksa Ipari Szakközépiskola 12.B
Gépipari Műszaki Középiskola
Szegedi Gépipari Technikum Erőmű Gépészeti Tagozatának Végzett Hallgatói

épület szintje: 1. emelet
készítették: Tóth-Szeles Norbert, Kiefer Dániel, Czégé Richárd

*/
 
 /*
    fájl meghatározásánál ügyeljenek a szóközökre!
    
    X,Y pozíciók meghatározásánál teljes képernyő, console alulra dokkolva,
    és próbálják ki, hogy mit ír ki a konzol egérkattintásra:
      a bal felső sarok kb. 0,0
      a jobb alsó sarok kb. 800,600
    --> csak ekkor lesz jó a konzolból leolvasott érték!
    
    nevek írásánál minden egyes tablok elem alatt párhuzamosan legyen egy tömbelem a neveknek. pl.:
    tablok[12] = [...];
    nevek[12] = "Alfa Áron, Béta Barnabás, Gamma Gábor";
 */
 
var tablok = [];
tablok[0] = ["Déri Miksa Ipari Szakközépiskola 12.D", 2002, 2006, "2006.D 001_1361.jpg", 635, 190, 1];
tablok[1] = ["Déri Miksa Gépipari Szakközépiskola Gépészeti Szak Levelező", 1978, 1982, "1982.lev. gépész  001_1366.jpg", 602, 180, 1];
tablok[2] = ["Déri Miksa Ipari Szakközépiskola 4.C", 1991, 1995, "1995.C 001_1370.jpg", 635, 175, 1];
tablok[3] = ["Szegedi Ipari Középiskola Déri Miksa Tagintézménye 12.b", 2006, 2010, "2010.B  -001_1372.jpg", 635, 150, 1];
tablok[4] = ["Szegedi Ipari Középiskola Déri Miksa Tagintézménye 12.c", 2006, 2010, "2010.C  001_1374.jpg", 635, 140, 1];
tablok[5] = ["Déri Miksa Ipari Szakközépiskola 4.c", 1990, 1994, "1994.C  001_1388.jpg", 635, 100, 1];
tablok[6] = ["Déri Miksa Ipari Szakközépiskola 12.d", 1999, 2003, "2003.D  001_1391.jpg", 635, 75, 1];
tablok[7] = ["Déri Miksa Gépipari Technikum 4.c", 1952, 1956, "1956.C  001_1414.jpg", 635, 13, 1];
tablok[8] = ["A Szegedi Gépipari Technikum 4.A", 1953, 1957, "1957.A  001_1412.jpg", 602, 13, 1];
tablok[9] = ["Déri Miksa Általános Gépipari Technikum Levelező Tagozat", 1957, 1961, "1961.lev.gép.  001_1410.jpg", 602, 30, 1];
tablok[10] = ["Déri Miksa Ipari Szakközépiskola 12.A", 2005, 2009, "2009.A  001_1397.jpg", 602, 40, 1];
tablok[11] = ["Déri Miksa Gépipari Technikum Esti", 1956, 1960, "1960.esti 001_1394.jpg", 602, 50, 1];
tablok[12] = ["Dólgozók Ipari Középiskolája Szeged Villamos Tagozatának Érettségizői", 1947, 1950, "1950. vill. 001_1393.jpg", 602, 65, 1];
tablok[13] = ["Gépipari Technikum Esti Villamos Tagozatának ", 1948, 1952, "1952. esti vill.  001_1386.jpg", 602, 80, 1];
tablok[14] = ["Szegedi Ipari Középiskola Déri Miksa Tagintézménye 12.A", 2006, 2010, "2010.A  001_1384.jpg", 602, 90, 1];		
tablok[15] = ["Szegedi Gépipari Technikum 4.C", 1949, 1953, "1953.C  001_1381.jpg", 602, 102, 1];
tablok[16] = ["Déri Miksa Ipari Szakközépiskola 12.B", 2002, 2006, "2006.B  001_1379.jpg", 602, 115, 1];
tablok[17] = ["Gépipari Műszaki Középiskola", 1947, 1951, "1951.felnőtt  dolg.  001_1377.jpg", 590, 115, 1];
tablok[18] = ["Szegedi Gépipari Technikum Erőmű Gépészeti Tagozatának Végzett Hallgatói", 1952, 1956, "1956.esti B   Erőmű gépész  001_1376.jpg", 578, 115, 1];

tablok_osszes = tablok_osszes.concat(tablok);